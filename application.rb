require 'grape'
require 'uuidtools'
module Master
  class API < Grape::API
    version 'v1' 
    format :json
    prefix :api

    resource :jobs do
      error_formatter :txt, ->(message, backtrace, options, env) {
        "error: #{message} from #{backtrace}"
      }

      desc 'create jobs'
      params do
        requires :cfg, type: Rack::Multipart::UploadedFile, desc: 'a cfg file that the worker runs'
      end
      post do
        io = params[:cfg]
        ret = []
        io.tempfile.each_line {|line|
          ret << line
        }
        uuid = UUIDTools::UUID.timestamp_create
        dir = 'jobs/' + uuid.to_s
        new_file_name = dir + '/cfg.json'
        FileUtils.mkdir_p dir
        FileUtils.cp io.tempfile.path, new_file_name
        dir
      end
    end
  end
end
